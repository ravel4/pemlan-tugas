import java.util.ArrayList;
import java.util.Scanner;

class Student {
    private String nim;
    private String nama;
    private String alamat;

    public Student(String nim, String nama, String alamat) {
        this.nim = nim;
        this.nama = nama;
        this.alamat = alamat;
    }

    public String getNim() {
        return nim;
    }

    public String getNama() {
        return nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public String toString() {
        return nim + " | " + nama + " | " + alamat;
    }
}

public class tugas2 {
    public static void main(String[] args) {
        ArrayList<Student> students = new ArrayList<>();
        Scanner in = new Scanner(System.in);

        boolean next = true;
        while (next) {
            System.out.print("masukkan nim : ");
            String nim = in.nextLine();

            System.out.print("masukkan nama : ");
            String nama = in.nextLine();

            System.out.print("masukkan alamat : ");
            String alamat = in.nextLine();

            Student student = new Student(nim, nama, alamat);
            students.add(student);

            System.out.print("tambah lagi? ");
            String tambah = in.nextLine();

            if (tambah.equalsIgnoreCase("t")) {
                next = false;
            }
        }

        System.out.println("==================================");
        for (Student student : students) {
            System.out.println(student);
        }
    }
}